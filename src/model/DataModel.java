package model;

import java.io.Serializable;
import java.util.List;

public class DataModel implements Serializable {
    /**
     * header of data object
     */
    private String status;

    /**
     * account is logged in system
     */
    private Account loggedAccount;

    /**
     * send account
     */
    private Account sendAccount;

    /**
     * receive account
     */
    private Account receiveAccount;

    /**
     * message from send account to receive account
     */
    private String messageContent;

    /**
     * online friends
     */
    private List<Account> onlineFriends;

    //other attributes and getter, setter methods here


    public DataModel() {
    }

    public DataModel(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Account getSendAccount() {
        return sendAccount;
    }

    public void setSendAccount(Account sendAccount) {
        this.sendAccount = sendAccount;
    }

    public Account getReceiveAccount() {
        return receiveAccount;
    }

    public void setReceiveAccount(Account receiveAccount) {
        this.receiveAccount = receiveAccount;
    }

    public List<Account> getOnlineFriends() {
        return onlineFriends;
    }

    public void setOnlineFriends(List<Account> onlineFriends) {
        this.onlineFriends = onlineFriends;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Account getLoggedAccount() {
        return loggedAccount;
    }

    public void setLoggedAccount(Account loggedAccount) {
        this.loggedAccount = loggedAccount;
    }
}
