package model;

import java.io.Serializable;

/**
 * @author tatsuya
 */
public class Account implements Serializable {
    private String userName;
    private String password;

    public Account() {
    }

    public Account(String username, String password) {
        this.userName = username;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return this.userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;
        return (this.getUserName().compareTo(((Account) o).getUserName())==0)?true:false;
    }

    @Override
    public int hashCode() {
        int result = userName != null ? userName.hashCode() : 0;
        return result;
    }
}