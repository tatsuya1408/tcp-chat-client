package model;

import javax.swing.*;
import java.io.Serializable;

public class ImageModel extends DataModel implements Serializable {
    private ImageIcon imageIcon;

    public ImageModel(){

    }

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(ImageIcon imageIcon) {
        this.imageIcon = imageIcon;
    }

}
