import controller.LoginController;
import view.LoginFrm;
import view.View;

public class ClientMain {
    public static void main(String[] args) {
        LoginFrm view = new LoginFrm();
        LoginController loginController = new LoginController(view);
    }
}
