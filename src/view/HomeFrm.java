/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.Account;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * @author tatsuya
 */
public class HomeFrm extends JFrame {

    private DefaultListModel<Account> listModel;

    /**
     * Creates new form HomeFrm
     */
    public HomeFrm() {
        initComponents();
        this.listModel = new DefaultListModel<>();
        this.listFriends.setModel(listModel);
//        setProfileLabel();
        this.setVisible(true);
    }

    public void setAvt(ImageIcon imageIcon, Account account){
        this.profileLabel.setIcon(imageIcon);
        this.accountLabel.setText(account.getUserName());
        this.jPanel1.revalidate();
        this.jPanel1.repaint();
    }
    public void setStatus(Account acc){
        this.accountLabel.setText(acc.getUserName());
    }

    public int showConfirmDialog(Account account) {
        int response = JOptionPane.showConfirmDialog(this,
                account.getUserName() + " wants to connect with you",
                "Add Friend",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        return response;
        /*
        0: yes
        1: no
         */
    }

    public void addUser(Account account) {
        this.listModel.addElement(account);
    }

    public void removeUser(String username) {
        this.listModel.removeElement(username);
    }

    public void setClickListener(MouseListener listener) {
        this.listFriends.addMouseListener(listener);
    }

    public void setGroupChatListener(ActionListener listener) {
        this.btnChatGroup.addActionListener(listener);
    }

    public void setAddFriendListener(ActionListener listener) {
        this.btnAddFriends.addActionListener(listener);
    }

    public void setRequestListener(ActionListener listener) {
        this.btnRequest.addActionListener(listener);
    }

    public Account getUserName() {
        return this.listFriends.getSelectedValue();
    }

    public void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    public List<Account> getMultipleSelectedItems() {
        List<Account> listAccount = this.listFriends.getSelectedValuesList();
        return listAccount;
    }

    public void setNumberOfRequest(int count) {
        this.btnRequest.setText(btnRequest.getText().substring(0, 7) + "(+" + count + ")");
    }
    public void applyQualityRenderingHints(Graphics2D g2d) {

        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

    }
//    public void setProfileLabel() {
//        try {
//            BufferedImage master = ImageIO.read(new File("/mnt/C0442BA1442B9964/Images/1009885_429677397226976_7143960689382790739_n.jpg"));
//
//            int diameter = Math.min(master.getWidth(), master.getHeight());
//            BufferedImage mask = new BufferedImage(master.getWidth(), master.getHeight(), BufferedImage.TYPE_INT_ARGB);
//
//            Graphics2D g2d = mask.createGraphics();
//            applyQualityRenderingHints(g2d);
//            g2d.fillOval(0, 0, diameter - 1, diameter - 1);
//            g2d.dispose();
//
//            BufferedImage masked = new BufferedImage(diameter, diameter, BufferedImage.TYPE_INT_ARGB);
//            g2d = masked.createGraphics();
//            applyQualityRenderingHints(g2d);
//            int x = (diameter - master.getWidth()) / 2;
//            int y = (diameter - master.getHeight()) / 2;
//            g2d.drawImage(master, x, y, null);
//            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_IN));
//            g2d.drawImage(mask, 0, 0, null);
//            g2d.dispose();
//            //this.profileLabel.setIcon(new ImageIcon(masked));
//            this.profileLabel = new JLabel(new ImageIcon("/home/tatsuya/Desktop/avt.png"));
//            this.add(profileLabel);
//            //JOptionPane.showMessageDialog(null, new JLabel(new ImageIcon(masked)));
//        } catch (IOException ex) {
//            Logger.getLogger(HomeFrm.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }



    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new JPanel();
        lbHomePage = new JLabel();
        jScrollPane1 = new JScrollPane();
        listFriends = new JList<>();
        btnAddFriends = new JButton();
        btnLogout = new JButton();
        jLabel1 = new JLabel();
        btnChatGroup = new JButton();
        btnRequest = new JButton();
        profileLabel = new JLabel();
        accountLabel = new JLabel();
        statusLabel = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(206, 218, 239));

        lbHomePage.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lbHomePage.setText("HOME PAGE");

        jScrollPane1.setViewportView(listFriends);

        btnAddFriends.setText("Add Friends");

        btnLogout.setText("Logout");

        jLabel1.setText("Online Friends");

        btnChatGroup.setText("Chat Group");

        btnRequest.setText("Requests");

        accountLabel.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        accountLabel.setForeground(new java.awt.Color(255, 0, 0));
        accountLabel.setText("Tatsuya");

        statusLabel.setText("Status: Online");

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(profileLabel, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(lbHomePage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(statusLabel, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(accountLabel, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddFriends)
                            .addComponent(btnChatGroup))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(btnRequest, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnLogout))
                        .addGap(26, 26, 26))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbHomePage, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(accountLabel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(statusLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(profileLabel, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)))
                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAddFriends, GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(btnRequest, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(btnChatGroup, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLogout, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HomeFrm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JLabel accountLabel;
    private JButton btnAddFriends;
    private JButton btnChatGroup;
    private JButton btnLogout;
    private JButton btnRequest;
    private JLabel jLabel1;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JLabel lbHomePage;
    private JList<Account> listFriends;
    private JLabel profileLabel;
    private JLabel statusLabel;
    // End of variables declaration//GEN-END:variables
}
