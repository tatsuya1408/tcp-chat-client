package view;

import model.Account;
import model.DataModel;

import java.util.Scanner;

public class View {
    public DataModel getDataModel() {
        Scanner sc = new Scanner(System.in);
        System.out.print("enter your username: ");
        String username = sc.nextLine();
        System.out.println("enter your password : ");
        String password = sc.nextLine();
        Account account = new Account(username, password);
        DataModel dataModel = new DataModel();
        dataModel.setLoggedAccount(account);
        dataModel.setSendAccount(account);
        return dataModel;
    }

    public void showMessage(String message) {
        System.out.println(message);
    }
}
