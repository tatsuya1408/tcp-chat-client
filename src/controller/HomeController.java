/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import global.HeaderConstants;
import model.Account;
import model.DataModel;
import model.ImageModel;
import model.MessageModel;
import view.ChatFrm;
import view.HomeFrm;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.swing.*;

/**
 * @author tatsuya
 */
public class HomeController {

    private HomeFrm homePage;
    private ObjectInputStream readFromServer;
    private ObjectOutputStream writeToServer;
    private Account loggedAccount;
    private ChatController singleChatController;
    //    private Map<Account, ChatController> listChat;

    private ImageIcon loggedIcon;
    private boolean lock = true;

    public HomeController(HomeFrm homePage, Account loggedAccount,
                          ObjectInputStream readFromServer, ObjectOutputStream writeToServer) {
        this.homePage = homePage;
        this.readFromServer = readFromServer;
        this.writeToServer = writeToServer;
        this.loggedAccount = loggedAccount;
        this.homePage.setTitle(loggedAccount.getUserName());
//        this.listChat = new ConcurrentHashMap<>();

        init();
        setListener();
        loadFriends();
        listening();
    }

    private void init() { // send request to server to get profile image
        ImageModel im = new ImageModel();
        im.setStatus(HeaderConstants.LOAD_PROFILE_IMAGE);
        im.setMessageContent(loggedAccount.getUserName());
        im.setReceiveAccount(loggedAccount);
        try {
            writeToServer.writeObject(im);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadFriends() {
        try {
            DataModel model = new DataModel(HeaderConstants.LOAD_FRIENDS);
            model.setLoggedAccount(loggedAccount);
            writeToServer.writeObject(model);
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listening() { // listening data from server
        new Thread(() -> {
            while (true) {
                try {
                    Object object = readFromServer.readObject();
                    if (object instanceof DataModel) {
                        String status = ((DataModel) object).getStatus();
                        System.out.println("Homepage: Status = " + status);

                        switch (status) {
                            case HeaderConstants.UPDATE_ONLINE_FR: { // update whenever a friend is online
                                homePage.addUser(((DataModel) object).getLoggedAccount());
                                break;
                            }

                            case HeaderConstants.LOAD_FRIENDS: { // all friends who are online
                                List<Account> friends = ((DataModel) object).getOnlineFriends();
                                friends.forEach(friend -> {
                                    homePage.addUser(friend);
                                });
                                break;
                            }

                            case HeaderConstants.CHAT_LOG: { // receive conversation history from server

                                break;
                            }

                            case HeaderConstants.CHAT: { // receive chat package from server
                                Account sendAccount = ((DataModel) object).getSendAccount();
                                String message = ((DataModel) object).getMessageContent();

                                if (singleChatController == null) {
                                    singleChatController = new ChatController(
                                            new ChatFrm(), readFromServer, writeToServer, loggedAccount, sendAccount
                                    );
                                }
                                singleChatController.appendMessage(sendAccount, message);
                                break;
                            }

                            case HeaderConstants.LOAD_PROFILE_IMAGE: { // profile image received from server
                                this.loggedIcon = ((ImageModel) object).getImageIcon();
                                this.homePage.setAvt(((ImageModel) object).getImageIcon(), loggedAccount);
                                break;
                            }

                        }
                    }
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }

    private void setListener() { // set event handle
        this.homePage.setClickListener(new MouseClickListenter());
    }

    class MouseClickListenter extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                Account receiveAccount = homePage.getUserName();
                if (lock) {
                    lock = false;
                    singleChatController = new ChatController(
                            new ChatFrm(), readFromServer, writeToServer, loggedAccount, receiveAccount
                    );
                }
//                //send package to server to get chatLog
//                MessageModel messageModel = new MessageModel();
//                messageModel.setSendAccount(loggedAccount);
//                messageModel.setReceiveAccount(receiveAccount);
//                messageModel.setStatus(HeaderConstants.GET_CHAT_LOG);
//                try {
//                    writeToServer.writeObject(messageModel);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
            }
        }
    }

    private String parseContentMessageLog(String s) {
        StringTokenizer stk = new StringTokenizer(s, "|");
        List<String> list = new ArrayList<>();
        while (stk.hasMoreTokens()) {
            list.add(stk.nextToken());
        }
        return list.get(list.size() - 1);
    }

    /**
     * send package to server to get chatLog between 2 user
     */
    private void getChatLog(Account sendAccount, Account receiveAccount) {
        MessageModel messageModel = new MessageModel();
        messageModel.setSendAccount(sendAccount);
        messageModel.setReceiveAccount(receiveAccount);
        messageModel.setStatus(HeaderConstants.GET_CHAT_LOG);
        try {
            writeToServer.writeObject(messageModel);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
