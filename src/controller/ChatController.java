/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import global.HeaderConstants;
import model.Account;
import model.DataModel;
import view.ChatFrm;
import view.EmotionFrm;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author tatsuya
 */
public class ChatController {
    private ChatFrm chatFrm;
    private ObjectInputStream readFromServer;
    private ObjectOutputStream writeToServer;
    private Account sendAccount;
    private Account receiveAccount;
    private ChatController c;
    private EmotionFrm emotionFrm;
    private boolean isOpen;

    private ImageIcon loggedIcon;
    private ImageIcon friendIcon;

    public ChatController(ChatFrm chatFrm, ObjectInputStream readFromServer, ObjectOutputStream writeToServer,
                          Account sendAccount, Account receiveAccount) {
        this.chatFrm = chatFrm;
        this.writeToServer = writeToServer;
        this.readFromServer = readFromServer;
        this.sendAccount = sendAccount;
        this.receiveAccount = receiveAccount;
        c = this;
        this.isOpen = false;
        this.chatFrm.setTitleChat(receiveAccount.getUserName());
        setActionListener();
        setEmotionListener();
    }

    public ImageIcon getLoggedIcon() {
        return loggedIcon;
    }

    public void setLoggedIcon(ImageIcon loggedIcon) {
        this.loggedIcon = loggedIcon;
    }

    public ImageIcon getFriendIcon() {
        return friendIcon;
    }

    public void setFriendIcon(ImageIcon friendIcon) {
        this.friendIcon = friendIcon;
    }

    /**
     * add message to GUI
     * @param account account which you want to display in GUI
     * @param s message content
     */
    public void appendMessage(Account account, String s) {

        if (account.equals(sendAccount)) {
            if (s.contains(".png")) {
                this.chatFrm.printEmotionMessage(loggedIcon, account, s);
            } else {
                //this.chatFrm.printMessage(">"+ account.getUserName()+": "+ s+"\n");
                this.chatFrm.printMessage(loggedIcon, account, s);
            }
        } else {
            if (s.contains(".png")) {
                this.chatFrm.printEmotionMessage(friendIcon, account, s);
            } else {
                //this.chatFrm.printMessage(">"+ account.getUserName()+": "+ s+"\n");
                this.chatFrm.printMessage(friendIcon, account, s);
            }
        }

    }

    private void appendRecursiveMessage(Account account, String s) {
        if (s.contains(".png")) {
            this.chatFrm.printEmotionMessage(loggedIcon, account, s);
        } else {
            //this.chatFrm.printMessage(">"+ account.getUserName()+": "+ s+"\n");
            this.chatFrm.printMessage(loggedIcon, account, s);
        }
    }

    private void setActionListener() {
        this.chatFrm.setActionLisener(new SendMessageListener());
    }

    public void setEmotionListener() {
        this.chatFrm.setEmotionListener(new EmotionListener());
    }

    class SendMessageListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String messageContent = chatFrm.getMessage();
            if (messageContent.compareTo("") == 0) {
                chatFrm.showDialog("you must type some characters!");
            } else {
                DataModel dataModel = new DataModel(HeaderConstants.CHAT);
                dataModel.setSendAccount(sendAccount);
                dataModel.setReceiveAccount(receiveAccount);
                dataModel.setMessageContent(chatFrm.getMessage());
                appendRecursiveMessage(sendAccount, chatFrm.getMessage());
                try {
                    writeToServer.writeObject(dataModel);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    class EmotionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (isOpen) {
                emotionFrm.displayWindow();
            } else {
                emotionFrm = new EmotionFrm(sendAccount, receiveAccount, writeToServer, c);
            }

        }
    }

    public ChatFrm getChatFrm() {
        return chatFrm;
    }
}
