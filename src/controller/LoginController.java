package controller;

import global.HeaderConstants;
import model.DataModel;
import utils.ConfigReader;
import view.HomeFrm;
import view.LoginFrm;
import view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class LoginController {
    private LoginFrm gui;
    private String serverHost;
    private int serverPort;

    private ConfigReader configReader = ConfigReader.getInstance();

    public LoginController(LoginFrm loginFrm) {
        this.serverHost = configReader.getProperty("server.host");
        this.serverPort = Integer.parseInt(configReader.getProperty("server.port"));
        this.gui = loginFrm;
        this.gui.setLoginActionListener(new LoginListener());
    }

    class LoginListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                Socket socket = new Socket(serverHost, serverPort);
                ObjectInputStream readFromServer = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream writeToServer = new ObjectOutputStream(socket.getOutputStream());

                DataModel dataModel = gui.getDataModel();
                dataModel.setStatus(HeaderConstants.LOGIN);
                writeToServer.writeObject(dataModel);

                Object object = readFromServer.readObject();
                if (object instanceof DataModel) {
                    String status = ((DataModel) object).getStatus();
                    switch (status) {
                        case HeaderConstants.LOGIN_OK: {
                            gui.showMessage("Login successfully!");
                            gui.hideWindow();
                            new HomeController(
                                    new HomeFrm(),
                                    dataModel.getLoggedAccount(),
                                    readFromServer,
                                    writeToServer
                            );
                            break;
                        }
                        case HeaderConstants.LOGIN_FAILED: {
                            gui.showMessage("Login failed!");
                            break;
                        }
                    }
                }

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    class RegisterListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
//            new RegisterController(new RegisterFrm());
        }
    }

}
