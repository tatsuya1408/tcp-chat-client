package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConfigReader {
    private static ConfigReader ourInstance = new ConfigReader();
    private static Properties properties;

    public static ConfigReader getInstance() {
        return ourInstance;
    }

    private ConfigReader() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream("config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }
}
